const express = require('express');
const mongoose =require('mongoose'); 
const dotenv = require("dotenv");
const bodyParser = require('body-parser');
const app = express();
const authUser = require('./routes/auth');
const usertask = require('./routes/task');
const postRouts = require('./routes/posts');
dotenv.config();

// databse connection
//const url ='mongodb://localhost/todoapp';
mongoose.connect(process.env.MONGO_CONNECTION_URL,
    {
        useNewUrlParser:true,
        useCreateIndex:true,
        useUnifiedTopology:true,
        useFindAndModify:true
    });
    
    const connection = mongoose.connection;
    connection.once('open',()=>{
        console.log('Database Connnected...');
    }).catch(err=>{
        console.log('Connection filed...')
    });

// middleware
app.use(bodyParser.json());
app.use(express.json());

app.use("/api/auth",authUser);
app.use("/api/posts",usertask);
//app.use('/api/posts',postRouts);

const PORT = 3000;
app.listen(PORT,()=>{
    console.log(`server listen on ${PORT}`)
}) 