const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
    userId:{
        type:String,
        require:true
    },
    taskname:{
        type:String,
        require:true,
        min:3,
        max:20,
    },
    description:{
        type:String,
        require:true,
        max:50,
    },
    iscompleated:{
        type:Boolean,
        require:true,
    },
},{timestamps:true});

module.exports = mongoose.model("Task",TaskSchema);