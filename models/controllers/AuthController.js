const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();

exports.register = async (req,res,next) =>{
    try{
        // generate new password
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password,salt);
        // create new user
        const newUser = new User({
         username:req.body.username,
         email:req.body.email,
         password:hashedPassword,
     });
     // save user are responsd
        const user = await newUser.save();
        res.status(200).json(user);
    }catch(err){
         res.status(500).json(err)
    }
 }

 

exports.login = async (req,res,next) =>{
    try{
        const user = await User.findOne({email:req.body.email});
        !user && res.status(404).json("user not found");

        const validPassword = await bcrypt.compare(req.body.password,user.password)
        !validPassword && res.status(400).json("wrong password");

        //create and assing a token
        const token = jwt.sign({_id:user._id},process.env.Token_SECRET)
        res.header('auth-token',token).send(token);
        // res.status(200).json(user);
    }catch(err){
        res.status(500).json(err)
    }
}