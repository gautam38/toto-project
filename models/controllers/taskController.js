const { response } = require('express');
const Task = require('../models/Task');

exports.addTask = async(req,res)=>{
    const newTask = new Task(req.body);
    try{
        const saveTask = await newTask.save();
        res.status(200).json(saveTask);
    }
    catch(err){
        res.status(500).json(err);
    }
  
     
}
exports.updateTask = async(req,res)=>{
    try{
        // console.log('ppppppp',req.params.id);
        const task = await Task.findById(req.body.task_id);
        //console.log('aaa',task);
        if(task.userId === req.body.userId){
            await task.updateOne({$set:req.body});
            res.status(200).json("this task is updated")
        }else{
            res.status(403).json("you can update only your task")
        }
    }catch(err){
        res.status(500).json(err);
    }
}

exports.taskDelete = async(req,res)=>{
    //return console.log('dddddddd')
    try{
         console.log('ppppppp',req.params.id);
        const task = await Task.findById(req.body.task_id);
        //console.log('aaa',task);
        if(task.userId === req.body.userId){
            await task.deleteOne();
            res.status(200).json("this task delete")
        }else{
            res.status(403).json("you can delete only your task")
        }
    }catch(err){
        res.status(500).json(err);
    }
}

exports.getAll = (req,res) =>{
    Task.find().then(tasks=>{
        res.send(tasks);
    }).catch(err=>{
        res.status(500).send({
            message:err.message || "Somethin went wrong"
        });
    });
};
