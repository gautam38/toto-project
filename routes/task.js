const router = require('express').Router();
const Task = require('../models/Task');
const taskController = require('../controllers/taskController');


router.post('/',taskController.addTask);
router.put('/update',taskController.updateTask);
router.delete('/delete',taskController.taskDelete);
router.get('/gettasks',taskController.getAll);


module.exports = router; 