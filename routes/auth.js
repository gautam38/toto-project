const router = require('express').Router();
const User = require("../models/User");
const bcrypt = require("bcrypt");
const AuthCountroller = require('../controllers/AuthController');

//Register
router.post('/register',AuthCountroller.register)
// login
router.post("/login",AuthCountroller.login)

module.exports = router;